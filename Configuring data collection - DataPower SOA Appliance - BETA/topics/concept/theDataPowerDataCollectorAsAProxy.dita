<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE concept PUBLIC "-//OASIS//DTD DITA Concept//EN" "concept.dtd">
<concept id="concept_dxy_hbr_ybb">
 <title>The DataPower data collector as a proxy</title>
 <prolog>
  <author>Nathan Finn</author>
  <copyright>
   <copyryear year="2017"/>
   <copyrholder>CIT</copyrholder>
  </copyright>
  <critdates>
   <created date="14/11/2017"/>
   <revised modified="16/11/2017"/>
   <revised modified="18/11/2017"/>
   <revised modified="19/11/2017"/>
   <revised modified="21/11/2017"/>
   <revised modified="23/11/2017"/>
   <revised modified="24/11/2017"/>
   <revised modified="25/11/2017"/>
   <revised modified="26/11/2017"/>
  </critdates>
  <metadata>
   <audience type="Public"/>
   <keywords conkeyref="keyword/kwd2"></keywords>
   <keywords conkeyref="keyword/kwd3"></keywords>
   <keywords conkeyref="keyword/kwd8"></keywords>   
   <keywords conkeyref="index/kw1"></keywords>
   <keywords conkeyref="index/kw4"></keywords>
   <keywords conkeyref="index/kw8"></keywords>
  </metadata>
 </prolog>
 <conbody>
  <p>Data collectors provided with ITCAM for SOA are usually installed directly into the application
   server runtime environment hosting the services being monitored. The DataPower® SOA appliance,
   however, does not support the installation of additional software, such as a data collector.
   Unlike other application server runtime environments, the ITCAM for SOA data collector for the
   DataPower environment is installed on a separate computer system and uses a special communication
   mechanism that allows external software applications to receive data from its internal
   transaction log.</p>
  <p>This communication mechanism is used to retrieve monitoring data about services requests
   flowing through one or more DataPower SOA appliances, and to convert the data into a format that
   ITCAM for SOA can process. In this way, the DataPower data collector acts as a proxy between the
   DataPower SOA appliances and the ITCAM for SOA monitoring agent.</p>
  <p>The DataPower data collector can be installed on a dedicated computer system, or it can run on
   a computer that is also hosting data collectors for other application server runtime
   environments.</p>
  <note type="important" rev="1.0">
   <p>IBM® supports only one instance of the DataPower data collector running on any computer
    system. This one data collector instance, however, can monitor any number of domains on any
    number of appliances, subject to available resources.</p>
  </note>
  <p>When data collection is enabled for the DataPower environment, the data collector subscribes to
   each monitored DataPower SOA appliance and then polls the appliance for monitoring data at the
   specified interval. The data that is retrieved from the DataPower SOA appliance is written to
   metric log files in the format used by ITCAM for SOA. When this data is later displayed in the
   Tivoli® Enterprise Portal, nodes are displayed in the Tivoli Enterprise Portal Navigator view
   that represent the DataPower SOA appliances that are being monitored. You can select workspaces
   under these nodes and view the services management data for the service requests flowing through
   the monitored DataPower SOA appliances.</p>
  <p>The DataPower data collector can subscribe to multiple DataPower SOA appliances, and retrieve
   and manage data from multiple domains. This data can then be separated by DataPower domain or
   aggregated across multiple domains and appliances, depending on how you configure the data
   collector. The DataPower data collector uses a configuration file that contains information about
   which DataPower SOA appliances are being monitored and information needed to establish
   communication with each monitored appliance.</p>
 </conbody>
</concept>
